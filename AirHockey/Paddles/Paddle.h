//
//  Paddle.h
//  AirHockey
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import <Foundation/Foundation.h>

@interface Paddle : NSObject
{
    UIView *view;    // paddle view with current position
    CGRect boundary; // confined boundary
    CGPoint pos;     // position paddle is moving to
    float maxSpeed;  // maximum speed
    float speed;     // current speed
    __unsafe_unretained UITouch *touch;  // touch assigned to this paddle
}

@property (assign) UITouch *touch;
@property (readonly) float speed;
@property (assign) float maxSpeed;

// initialize object
-(id) initWithView: (UIView*) paddle Boundary: (CGRect) rect
          MaxSpeed: (float) max;

// reset position to middle of boundary
-(void) reset;

// set where the paddle should move to
-(void) move: (CGPoint) pt;

// center point of paddle
-(CGPoint) center;

// check if the paddle intersects with the rectangle
-(bool) intersects: (CGRect) rect;

// get distance between current paddle position and point
-(float) distance: (CGPoint) pt;

// animate puck view to next position without exceeding max speed
-(void) animate;

@end