//
//  PaddlesAppDelegate.h
//  Paddles
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import <UIKit/UIKit.h>

@class PaddlesViewController;

@interface PaddlesAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) PaddlesViewController *viewController;

@end
