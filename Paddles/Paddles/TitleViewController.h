//
//  TitleViewController.h
//  Paddles
//
//  Created by Todd Moore on 8/30/11.
//  Copyright 2011 TMSOFT. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PaddlesViewController;

@interface TitleViewController : UIViewController
{
}

@property (nonatomic, retain) PaddlesViewController *gameController;

- (IBAction)onPlay:(id)sender;
- (IBAction)onBook:(id)sender;
- (void)stopGame;

@end
