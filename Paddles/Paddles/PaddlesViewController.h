//
//  PaddlesViewController.h
//  Paddles
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import <UIKit/UIKit.h>
#import "AudioToolbox/AudioToolbox.h"

@interface PaddlesViewController : UIViewController 
{
    UIView *viewPaddle1;
    UIView *viewPaddle2;
    UIView *viewPuck;
    UILabel *viewScore1;
    UILabel *viewScore2;
    
    UITouch *touch1;
    UITouch *touch2;
    
    float dx;
    float dy;
    float speed;
    
    NSTimer *timer;
    
    UIAlertView *alert;
    
    SystemSoundID sounds[3];
}
@property (nonatomic, retain) IBOutlet UIView *viewPaddle1;
@property (nonatomic, retain) IBOutlet UIView *viewPaddle2;
@property (nonatomic, retain) IBOutlet UIView *viewPuck;
@property (nonatomic, retain) IBOutlet UILabel *viewScore1;
@property (nonatomic, retain) IBOutlet UILabel *viewScore2;

- (void)resume;
- (void)pause;

@end
