//
//  TitleViewController.m
//  Paddles
//
//  Created by Todd Moore on 8/30/11.
//  Copyright 2011 TMSOFT. All rights reserved.
//

#import "TitleViewController.h"
#import "PaddlesViewController.h"

@implementation TitleViewController

@synthesize gameController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)onPlay:(id)sender 
{
    self.gameController = [[PaddlesViewController alloc] initWithNibName:@"PaddlesViewController" bundle:nil];
    [self presentModalViewController: gameController animated:NO];
}

- (IBAction)onBook:(id)sender 
{
    // open the link
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://toddmoore.com/book/"]];	
}

- (void)stopGame
{
    if (self.gameController)
    {
        [self.gameController dismissModalViewControllerAnimated: FALSE];
        self.gameController = nil;
    }
}

@end
