//
//  PaddlesAppDelegate.h
//  Paddles
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import <UIKit/UIKit.h>

@class PaddlesViewController;
@class TitleViewController;

@interface PaddlesAppDelegate : NSObject <UIApplicationDelegate> 
{

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet TitleViewController *viewController;

@end
