//
//  PaddlesViewController.m
//  Paddles
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import "PaddlesViewController.h"
#import "PaddlesAppDelegate.h"
#import "TitleViewController.h"

#define MAX_SCORE 3

@implementation PaddlesViewController
@synthesize viewPaddle1;
@synthesize viewPaddle2;
@synthesize viewPuck;
@synthesize viewScore1;
@synthesize viewScore2;

#define SOUND_WALL   0
#define SOUND_PADDLE 1
#define SOUND_SCORE  2

// load a sound effect into index of the sounds array
- (void)loadSound: (NSString*) name Slot: (int) slot
{
    if (sounds[slot] != 0) return;
    
    // Create pathname to sound file
    NSString *sndPath = [[NSBundle mainBundle] 
                                   pathForResource: name 
                                            ofType: @"wav" 
                                       inDirectory: @"/"];
    
    // Create system sound ID into our sound slot
    AudioServicesCreateSystemSoundID((__bridge CFURLRef) [NSURL fileURLWithPath: sndPath], &sounds[slot]);
}

- (void)initSounds
{
    [self loadSound: @"wall" Slot: SOUND_WALL];
    [self loadSound: @"paddle" Slot: SOUND_PADDLE];
    [self loadSound: @"score" Slot: SOUND_SCORE];
}

- (int)gameOver
{
    if ([viewScore1.text intValue] >= MAX_SCORE) return 1;
    if ([viewScore2.text intValue] >= MAX_SCORE) return 2;
    return 0;
}

- (void)playSound: (int) slot
{
    AudioServicesPlaySystemSound(sounds[slot]);
}

- (void)reset
{
    // set direction of ball to either left or right direction
    if ((arc4random() % 2) == 0) dx = -1; else dx = 1;
    
    // reverse dy if not 0 as this will send the puck to the 
    // player who just scored otherwise set in random direction
    if (dy != 0) dy = -dy;
        else if ((arc4random() % 2) == 0) dy = -1; else dy = 1;
    
    // move point to random position in center
    viewPuck.center = CGPointMake(15 + arc4random() % (int) (self.view.frame.size.width-30),
                                  self.view.frame.size.height/2);
    
    // reset speed
    speed = 2;
}

- (void)start
{
    if (timer == nil)
    {
        // create our animation timer
        timer = [NSTimer scheduledTimerWithTimeInterval: 1.0/60.0
                                     target: self 
                                   selector: @selector(animate) 
                                   userInfo: NULL 
                                    repeats: YES];
    }
    
    // show the puck
    viewPuck.hidden = NO;
}

- (void)stop
{
    if (timer != nil)
    {
        [timer invalidate];
        timer = nil;
    }
    
    // hide the puck
    viewPuck.hidden = YES;
}

- (void)displayMessage: (NSString*) msg
{
    // do not display more than one message
    if (alert) return;
    
    // stop animation timer
    [self stop];
    
    // create and show alert message
    alert = [[UIAlertView alloc] initWithTitle: @"Game" 
                                       message: msg
                                      delegate: self 
                             cancelButtonTitle: @"OK" 
                             otherButtonTitles: nil];
    [alert show];
}

// start a new game
- (void)newGame
{
    [self reset];
    
    // reset score
    viewScore1.text = @"0";
    viewScore2.text = @"0";
    
    // present message to start game
    [self displayMessage: @"Ready to play?"];
}

- (void)alertView:(UIAlertView *)alertView 
             didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // message dismissed so reset our game and start animation
    alert = nil;
    
    // check if we should start a new game
    if ([self gameOver])
    {
//        [self newGame];
        // switch back to title
        PaddlesAppDelegate *app = (PaddlesAppDelegate*) [UIApplication sharedApplication].delegate;
        [app.viewController stopGame];
        return;
    }
    
    // reset round
    [self reset];
    
    // start animation
    [self start];
}

- (BOOL)checkPuckCollision: (CGRect) rect 
                      DirX: (float) x 
                      DirY: (float) y
{
    // check if the puck intersects with rectangle passed
    if (CGRectIntersectsRect(viewPuck.frame, rect))
    {
        // change the direction of the ball
        if (x != 0) dx = x;
        if (y != 0) dy = y;
        return TRUE;
    }
    return FALSE;
}

- (BOOL)checkGoal
{
    // check if ball is out of bounds and reset game if so
    if (viewPuck.center.y < 0 || viewPuck.center.y >= self.view.frame.size.height)
    {
        // get integer value from score label
        int s1 = [viewScore1.text intValue];
        int s2 = [viewScore2.text intValue];
        
        // give a point to correct player
        if (viewPuck.center.y < 0) ++s2; else ++s1;
        
        // update score labels
        viewScore1.text = [NSString stringWithFormat: @"%u",s1];
        viewScore2.text = [NSString stringWithFormat: @"%u",s2];
        
        // check for winner
        if ([self gameOver] == 1)
        {
            // report winner
            [self displayMessage: @"Player 1 has won!"];
        }
        else if ([self gameOver] == 2)
        {
            // report winner
            [self displayMessage: @"Player 2 has won!"];
        }
        else
        {
            // reset round
            [self reset];
        }
        
        // return TRUE for goal
        return TRUE;
    }
    
    // no goal
    return FALSE;
}

- (void)increaseSpeed
{
    speed += 0.5;
    if (speed > 10) speed = 10;
}

// animate the puck and check for collisions
- (void)animate
{
    // move puck to new position based on direction and speed
    viewPuck.center = CGPointMake(viewPuck.center.x + dx*speed,
                                  viewPuck.center.y + dy*speed);
    
    // check puck collision with left and right walls
    if ([self checkPuckCollision: CGRectMake(-10, 0, 20, self.view.frame.size.height)
                            DirX: fabs(dx) DirY: 0])
    {
        // play hitting wall sound
        [self playSound: SOUND_WALL];
    }
    if ([self checkPuckCollision: CGRectMake(self.view.frame.size.width-10, 0, 20, self.view.frame.size.height)
                            DirX: -fabs(dx) DirY: 0])
    {
        // play hitting wall sound
        [self playSound: SOUND_WALL];        
    }
    
    // check puck collision with player paddles
    if ([self checkPuckCollision: viewPaddle1.frame 
                            DirX: (viewPuck.center.x - 
                                   viewPaddle1.center.x) / 32.0 
                            DirY: 1])
    {
        // play hitting paddle sound and increase speed
        [self increaseSpeed];
        [self playSound: SOUND_PADDLE];
    }
    if ([self checkPuckCollision: viewPaddle2.frame 
                            DirX: (viewPuck.center.x - 
                                   viewPaddle2.center.x) / 32.0 
                            DirY: -1])
    {
        // play hitting paddle sound and increase speed
        [self increaseSpeed];
        [self playSound: SOUND_PADDLE];    
    }
    
    // check for goal
    if ([self checkGoal])
    {
        // play scoring sound
        [self playSound: SOUND_SCORE];
    }
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // iterate through our touch elements
    for (UITouch *touch in touches)
    {
        // get the point of touch within the view
        CGPoint touchPoint = [touch locationInView: self.view];
        
        // check which half of the screen touch is on and assign
        // it to a specific paddle if not already assigned
        if (touch1 == nil && touchPoint.y < self.view.frame.size.height/2)
        {
            touch1 = touch;
            viewPaddle1.center = CGPointMake(touchPoint.x, 
                                     viewPaddle1.center.y);
        }
        else if (touch2 == nil && touchPoint.y >= self.view.frame.size.height/2)
        {
            touch2 = touch;
            viewPaddle2.center = CGPointMake(touchPoint.x, 
                                     viewPaddle2.center.y);
        }
    }
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    // iterate through our touch elements
    for (UITouch *touch in touches)
    {
        // get the point of touch within the view
        CGPoint touchPoint = [touch locationInView: self.view];
        
        // if the touch is assigned to our paddle then move it
        if (touch == touch1) 
        {
            viewPaddle1.center = CGPointMake(touchPoint.x, 
                                      viewPaddle1.center.y);
        }
        else if (touch == touch2)
        {
            viewPaddle2.center = CGPointMake(touchPoint.x, 
                                     viewPaddle2.center.y);
        }
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    // iterate through our touch elements
    for (UITouch *touch in touches)
    {
        if (touch == touch1) touch1 = nil;
        else if (touch == touch2) touch2 = nil;
    }
}
- (void)touchesCancelled:(NSSet *)touches 
               withEvent:(UIEvent *)event
{
    [self touchesEnded:touches withEvent:event];
}

- (void)pause
{
    [self stop];
}

- (void)resume
{
    // present a mesage to continue game
    [self displayMessage: @"Game Paused"];
}

// shake logic
- (BOOL)canBecomeFirstResponder
{ 
    return YES; 
}
-(void)viewDidAppear:(BOOL)animated 
{
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
}
- (void)viewWillDisappear:(BOOL)animated 
{
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
}

// debug motion
/*
 - (void)motionBegan:(UIEventSubtype)motion 
 withEvent:(UIEvent *)event 
 {
 if (event.type == UIEventSubtypeMotionShake) 
 {
 NSLog(@"Shake Began");
 }
 }
 - (void)motionEnded:(UIEventSubtype)motion 
 withEvent:(UIEvent *)event 
 {
 if (event.type == UIEventSubtypeMotionShake) 
 {
 NSLog(@"Shake Ended");
 }
 }
 - (void)motionCancelled:(UIEventSubtype)motion 
 withEvent:(UIEvent *)event 
 {
 if (event.type == UIEventSubtypeMotionShake) 
 {
 NSLog(@"Shake Cancelled");
 }
 }*/

- (void)motionBegan:(UIEventSubtype)motion 
          withEvent:(UIEvent *)event 
{
    if (event.type == UIEventSubtypeMotionShake) 
    {
        // pause game then resume to display message
        [self pause];
        [self resume];
    }
}

- (void)dealloc
{
    // dispose of sounds
    for (int i = 0; i < 3; ++i) 
    {
        AudioServicesDisposeSystemSoundID(sounds[i]);
    }
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initSounds];
    
    [self newGame];
}

- (void)viewDidUnload
{
    [self setViewPaddle1:nil];
    [self setViewPaddle2:nil];
    [self setViewPuck:nil];
    [self setViewScore1:nil];
    [self setViewScore2:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
