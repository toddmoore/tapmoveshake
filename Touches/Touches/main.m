//
//  main.m
//  Touches
//
//  Created by Todd Moore on 4/21/11.
//  http://toddmoore.com/
//

#import <UIKit/UIKit.h>
#include "TouchesAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool
    {
        return UIApplicationMain(argc,
                                 argv,
                                 nil,
                                 NSStringFromClass([TouchesAppDelegate class]));
    }
}
