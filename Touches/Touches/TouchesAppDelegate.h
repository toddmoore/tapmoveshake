//
//  TouchesAppDelegate.h
//  Touches
//
//  Created by Todd Moore on 4/21/11.
//  http://toddmoore.com/
//

#import <UIKit/UIKit.h>

@class TouchesViewController;

@interface TouchesAppDelegate : NSObject <UIApplicationDelegate> 
{

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet TouchesViewController *viewController;

@end
