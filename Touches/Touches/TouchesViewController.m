//
//  TouchesViewController.m
//  Touches
//
//  Created by Todd Moore on 4/21/11.
//  http://toddmoore.com/
//  Demonstrates multitouch and how touches are offset towards the status bar.
//

#import "TouchesViewController.h"

@implementation TouchesViewController

// ARC enabled
//- (void)dealloc
//{
//    [super dealloc];
//}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (YES);
}


// touches began will create new touch views to match touch points
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	// iterate through our touch elements
	for (UITouch *touch in touches)
	{
		// get the point of touch within the view
		CGPoint touchPoint = [touch locationInView: self.view];
        
        // add a new touch subview at touch position and tag it
        UIImageView *touchView = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"touch.png"]];
        touchView.center = touchPoint;
        touchView.tag = (int) touch;
        [self.view addSubview: touchView];
       
        // ARC enabled
        //[touchView release];
	}	
}

// touches moved will update touch view positions
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	// iterate through our touch elements and update sub views
	for (UITouch *touch in touches)
	{
        // get the point of touch within the view
		CGPoint touchPoint = [touch locationInView: self.view];
        
        for (UIView *touchView in self.view.subviews)
        {
            if (touchView.tag == (int) touch) 
            {
                touchView.center = touchPoint;
                break;
            }
        }
	}	
}

// touches end needs to removed any added touch views
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	for (UITouch *touch in touches)
	{
        for (UIView *touchView in self.view.subviews)
        {
            if (touchView.tag == (int) touch)
            {
                [touchView removeFromSuperview];
                break;
            }
        }
	}
}

// touches cancelled has same behavior as touches ended
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self touchesEnded: touches withEvent:event];
}

@end
