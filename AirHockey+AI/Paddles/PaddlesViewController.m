//
//  PaddlesViewController.m
//  Paddles
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import "PaddlesViewController.h"
#import "PaddlesAppDelegate.h"

#define MAX_SCORE 3

#define SOUND_WALL 0
#define SOUND_PADDLE 1
#define SOUND_SCORE 2

#define MAX_SPEED 15
struct CGRect gPlayerBox[] =
{   // x, y    width, height
    { 40, 40, 320-80, 240-40-32 }, // player 1 box
    { 40, 240+33, 320-80, 240-40-32 } // player 2 box
};

// puck is contained by this rect
struct CGRect gPuckBox =
{ // x, y    width, height
    28, 28, 320-56, 480-56
};

// goal boxes that puck can enter
struct CGRect gGoalBox[] =
{
    { 102, -20, 116, 49 }, // player 1 win box
    { 102, 451, 116, 49 }  // player 2 win box
};

@implementation PaddlesViewController
@synthesize viewPaddle1;
@synthesize viewPaddle2;
@synthesize viewPuck;
@synthesize viewScore1;
@synthesize viewScore2;
@synthesize debug;
@synthesize computer;

// load a sound effect into index of the sounds array
- (void)loadSound: (NSString*) name Slot: (int) slot
{
    if (sounds[slot] != 0) return;
    // Create pathname to sound file
    NSString *sndPath = [[NSBundle mainBundle]
                         pathForResource: name
                         ofType: @"wav"
                         inDirectory: @"/"];
    // Create system sound ID into our sound slot
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)
                                     [NSURL fileURLWithPath: sndPath], &sounds[slot]);
}
- (void)initSounds
{
    [self loadSound: @"wall" Slot: SOUND_WALL];
    [self loadSound: @"paddle" Slot: SOUND_PADDLE];
    [self loadSound: @"score" Slot: SOUND_SCORE];
}

- (void)playSound: (int) slot
{
    AudioServicesPlaySystemSound(sounds[slot]);
}

- (int)gameOver
{
    if ([viewScore1.text intValue] >= MAX_SCORE) return 1;
    if ([viewScore2.text intValue] >= MAX_SCORE) return 2;
    return 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (void)reset
{
    // reset paddles and puck
    [paddle1 reset];
    [paddle2 reset];
    [puck reset];
    
    // reset computer AI
    state = 0;
    
    // test puck trap issue
    //viewPuck.center = CGPointZero;
}

- (void)start
{
    if (timer == nil)
    {
        // create our animation timer
        timer = [NSTimer
                  scheduledTimerWithTimeInterval: 1.0/60.0
                  target: self
                  selector: @selector(animate)
                  userInfo: NULL
                  repeats: YES];
    }
    // show the puck
    viewPuck.hidden = NO;
}

- (void)stop
{
    if (timer != nil)
    {
        [timer invalidate];
        //[timer release];
        timer = nil;
    }
    // hide the puck
    viewPuck.hidden = YES;
}

- (void)displayMessage: (NSString*) msg
{
    // do not display more than one message
    if (alert) return;
    // stop animation timer
    [self stop];
    // create and show alert message
    alert = [[UIAlertView alloc] initWithTitle: @"Game"
                                       message: msg
                                      delegate: self
                             cancelButtonTitle: @"OK"
                             otherButtonTitles: nil];
    [alert show];
    //[alert release];
}

- (void)newGame
{
    [self reset];
    // reset score
    viewScore1.text = @"0";
    viewScore2.text = @"0";
    
    // present message to start game
    [self displayMessage: @"Ready to play?"];
}

- (void)alertView:(UIAlertView *)alertView
        didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // message dismissed so reset the game and start animation
    alert = nil;
    
    // check if we should go back to title
    if ([self gameOver])
    {
        PaddlesAppDelegate *app = (PaddlesAppDelegate*) [UIApplication sharedApplication].delegate;
        [app showTitle];
        return;
    }
    
    // reset round
    [self reset];
    
    // start animation
    [self start];
}


- (BOOL) checkGoal
{
    // check if ball is out of bounds and reset game if so
    if (puck.winner != 0)
    {
        // get integer value from score label
        int s1 = [viewScore1.text intValue];
        int s2 = [viewScore2.text intValue];
        // give a point to correct player
        if (puck.winner == 2) ++s2; else ++s1;
        // update score labels
        viewScore1.text = [NSString stringWithFormat: @"%u", s1];
        viewScore2.text = [NSString stringWithFormat: @"%u", s2];
        // check for winner
        if ([self gameOver] == 1)
        {
            // report winner
            [self displayMessage: @"Player 1 has won!"];
        }
        else if ([self gameOver] == 2)
        {
            // report winner
            [self displayMessage: @"Player 2 has won!"];
        }
        else
        {
            // reset round
            [self reset];
        }

        // return TRUE for goal
        return TRUE;
    }
    // no goal
    return FALSE;
}

- (void) computerAI
{
    if (state == AI_START)
    {
        debug.text = @"START";
        if (paddle2.speed > 0 ||
            (arc4random() % (100/computer)) == 1)
        {
            state = AI_WAIT;
        }
    }
    else if (state == AI_WAIT)
    {
        // fix to handle computer trapping puck into the corner
        if ([paddle1 intersects: viewPuck.frame])
        {
            // go into a bored state so paddle moves to
            // random position
            state = AI_BORED;
            return;
        }
        // wait until paddle has stopped
        if (paddle1.speed == 0)
        {
            debug.text = @"WAIT";
            paddle1.maxSpeed = MAX_SPEED;
            
            int r = arc4random() % ((4 - computer) *4);
            // if we pick the number 1 then we go into a
            // new state
            if (r == 1)
            {
                // if puck is on our side and not moving fast
                // go into offense. if puck is heading
                // upwards with some speed go into defense.
                // otherwise get bored
                if (puck.center.y <= 240 && puck.speed < computer)
                {
                    if (computer == 1) state = AI_OFFENSE2;
                        else state = AI_OFFENSE;
                }
                else if (puck.speed >= 1 && puck.dy < 0)
                {
                    state = AI_DEFENSE;
                }
                else
                {
                    state = AI_BORED;
                }
            }
        }
    }
    else if (state == AI_OFFENSE)
    {
        debug.text = @"OFFENSE";
        if (computer < 3) paddle1.maxSpeed = MAX_SPEED / 2;

        // pick a new x position between -64 and +64
        // of puck center
        float x = puck.center.x - 64 + (arc4random() % 129);
        float y = puck.center.y - 64 - (arc4random() % 64);
        [paddle1 move: CGPointMake(x,y)];
        state = AI_OFFENSE2;
    }
    else if (state == AI_OFFENSE2)
    {
        debug.text = @"OFFENSE2";
        if (computer == 1)
        {
            paddle1.maxSpeed = MAX_SPEED / 2;
        } 
        else if (computer == 2)
        {
            paddle1.maxSpeed = MAX_SPEED * 3/4;
        }
        // strike it
        [paddle1 move: puck.center];
        state = AI_WAIT;
    }
    else if (state == AI_DEFENSE)
    {
        debug.text = @"DEFENSE";
        // move to the puck x position and split the difference
        // between the goal
        float offset = ((puck.center.x - 160.0) / 160.0) * 40.0;
        [paddle1 move: CGPointMake(puck.center.x - offset,
                                   puck.center.y / 2) ];
        if (puck.speed < 1 || puck.dy > 0)
        {
            state = AI_WAIT;
        }
        if (computer == 1)
        {
            paddle1.maxSpeed = MAX_SPEED / 3;
        } 
        else if (computer == 2)
        {
            paddle1.maxSpeed = MAX_SPEED * 2/5;
        } 
        else if (computer == 3)
        {
            paddle1.maxSpeed = MAX_SPEED / 2;
        }
    }
    // computer is bored and moves to random position
    else if (state == AI_BORED)
    {
        if (paddle1.speed == 0)
        {
            debug.text = @"BORED";
            // change paddle speed based on level
            paddle1.maxSpeed = 3 + computer;
            
            // inset the rectangle if medium (20) or hard (40)
            int inset = (computer - 1) * 20;
            
            // move paddle into a random position within the
            // player1 box
            float x = (gPlayerBox[0].origin.x + inset) +
                       arc4random() % (int) (gPlayerBox[0].size.width - inset*2);
            float y = gPlayerBox[0].origin.y +
                       arc4random() % (int) (gPlayerBox[0].size.height - inset);
            
            [paddle1 move: CGPointMake(x,y)];
            state = AI_WAIT;
        }
    }
}

// animate the puck and check for collisions
- (void) animate
{
    // check for computer player
    if (computer)
    {
        [self computerAI];
    }
    
    // move paddles
    [paddle1 animate];
    [paddle2 animate];
    
    // Handle paddles collisions which return true if a collision
    // occurred
    if ([puck handleCollision: paddle1] ||
        [puck handleCollision: paddle2])
    {
        // play paddle hit
        [self playSound: SOUND_PADDLE];
    }
    
    // animate our puck which returns true if a wall was hit
    if ([puck animate])
    {
        [self playSound: SOUND_WALL];
    }
    
    // Check for goal
    if ([self checkGoal])
    {
        [self playSound: SOUND_SCORE];
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initSounds];
    
    /*
    // debug code to show player boxes
    for (int i = 0; i < 2; ++i)
    {
        UIView *view = [[UIView alloc] initWithFrame:
                        gPlayerBox[i] ];
        view.backgroundColor = [UIColor redColor];
        view.alpha = 0.25;
        [self.view addSubview: view];
        [view release];
    }
    
    // debug code to show goal boxes
    for (int i = 0; i < 2; ++i)
    {
        UIView *view = [[UIView alloc] initWithFrame:
                        gGoalBox[i] ];
        view.backgroundColor = [UIColor greenColor];
        view.alpha = 0.25;
        [self.view addSubview: view];
        [view release];
    }
    
    // debug code to show puck box
    UIView *view = [[UIView alloc] initWithFrame: gPuckBox ];
    view.backgroundColor = [UIColor grayColor];
    view.alpha = 0.25;
    [self.view addSubview: view];
    [view release];
    */
    
    // create the paddle helpers
    paddle1 = [[Paddle alloc] initWithView: viewPaddle1
                                  Boundary: gPlayerBox[0]
                                  MaxSpeed: MAX_SPEED];
    
    paddle2 = [[Paddle alloc] initWithView: viewPaddle2
                                  Boundary:gPlayerBox[1]
                                  MaxSpeed: MAX_SPEED];

    puck = [[Puck alloc] initWithPuck:viewPuck
                             Boundary:gPuckBox
                                Goal1:gGoalBox[0]
                                Goal2:gGoalBox[1]
                             MaxSpeed: MAX_SPEED];
    
    [self newGame];
    
    debug.hidden = YES;
}

- (void)viewDidUnload
{
    // free helpers
    //[paddle1 release];
    //[paddle2 release];
    //[puck release];
    
    [self setViewPaddle1:nil];
    [self setViewPaddle2:nil];
    [self setViewPuck:nil];
    [self setViewScore1:nil];
    [self setViewScore2:nil];
    [self setDebug:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // iterate through our touch elements
    for (UITouch *touch in touches)
    {
        // get the point of touch within the view
        CGPoint touchPoint = [touch locationInView: self.view];
        // if paddle not already assigned a specific touch then
        // determine which half of the screen the touch is on
        // and assign it to that specific paddle
        if (paddle1.touch == nil && touchPoint.y < 240 && computer == 0)
        {
            touchPoint.y += 48;
            paddle1.touch = touch;
            [paddle1 move: touchPoint];
        }
        else if (paddle2.touch == nil && touchPoint.y >= 240)
        {
            touchPoint.y -= 32;
            paddle2.touch = touch;
            [paddle2 move: touchPoint];
        }
    }
}

// handle touch move events
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    // iterate through our touch elements
    for (UITouch *touch in touches)
    {
        // get the point of touch within the view
        CGPoint touchPoint = [touch locationInView: self.view];
        // if paddle not already assigned a specific touch then
        // determine which half of the screen the touch is on
        // and assign it to that specific paddle
        if (paddle1.touch == touch)
        {
            touchPoint.y += 48;
            [paddle1 move: touchPoint];
        }
        else if (paddle2.touch == touch)
        {
            touchPoint.y -= 32;
            [paddle2 move: touchPoint];
        }
    }
}

// handle touches end events
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    // iterate through our touch elements
    for (UITouch *touch in touches)
    {
        if (paddle1.touch == touch) paddle1.touch = nil;
        else if (paddle2.touch == touch) paddle2.touch = nil;
    }
}

- (void)touchesCancelled:(NSSet *)touches
               withEvent:(UIEvent *)event
{
    [self touchesEnded:touches withEvent:event];
}

/*
- (void)motionBegan:(UIEventSubtype)motion
          withEvent:(UIEvent *)event
{
    if (event.type == UIEventSubtypeMotionShake)
    {
        NSLog(@"Shake Began");
    }
}
- (void)motionEnded:(UIEventSubtype)motion
          withEvent:(UIEvent *)event
{
    if (event.type == UIEventSubtypeMotionShake)
    {
        NSLog(@"Shake Ended");
    }
}
- (void)motionCancelled:(UIEventSubtype)motion
              withEvent:(UIEvent *)event
{
    if (event.type == UIEventSubtypeMotionShake)
    {
        NSLog(@"Shake Cancelled");
    }
}
*/
- (void)motionBegan:(UIEventSubtype)motion
          withEvent:(UIEvent *)event
{
    if (event.type == UIEventSubtypeMotionShake)
    {
        // pause game then resume to display message
        [self pause];
        [self resume];
    }
}

/*
- (BOOL)shouldAutorotateToInterfaceOrientation: 
        (UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for portait only
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)dealloc 
{
    // free helpers
    //[paddle1 release];
    //[paddle2 release];
    //[puck release];
    
    // dispose of sounds
    for (int i = 0; i < 3; ++i)
    {
        AudioServicesDisposeSystemSoundID(sounds[i]);
    }
    
    //[viewPaddle1 release];
    //[viewPaddle2 release];
    //[viewPuck release];
    //[viewScore1 release];
    //[viewScore2 release];
    //[debug release];
    //[super dealloc];
}

- (void)pause
{
    [self stop];
}

- (void)resume
{
    // present a mesage to continue game
    [self displayMessage: @"Game Paused"];
}

@end
