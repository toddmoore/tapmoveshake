//
//  TitleViewController.m
//  AirHockey
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import "TitleViewController.h"
#import "PaddlesAppDelegate.h"

@implementation TitleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)onPlay:(id)sender 
{
    PaddlesAppDelegate *app = (PaddlesAppDelegate*)
      [UIApplication sharedApplication].delegate;
    UIButton *button = (UIButton*) sender;
    [app playGame: (int) button.tag];
}

// ARC enabled
//- (void)dealloc {
//    [super dealloc];
//}
@end
