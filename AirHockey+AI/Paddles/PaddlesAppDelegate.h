//
//  PaddlesAppDelegate.h
//  Paddles
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import <UIKit/UIKit.h>

@class PaddlesViewController;
@class TitleViewController;

@interface PaddlesAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) TitleViewController *viewController;
@property (strong, nonatomic) PaddlesViewController *gameController;

- (void)showTitle;
- (void)playGame: (int) computer;

@end
