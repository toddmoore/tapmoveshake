//
//  Puck.h
//  AirHockey
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import <Foundation/Foundation.h>
#import "paddle.h"

@interface Puck : NSObject
{
    UIView *view;   // puck view this object controls
    CGRect rect[3]; // contains our boundary, goal1, and goal2 rects
    int box;        // box the puck is confined to (index into rects)
    float maxSpeed; // maximum speed of puck
    float speed;    // current speed of puck
    float dx, dy;   // current direction of puck
    int winner;     // declared winner (0=none, 1=player 1 won
                    // (0=none, 1=player 1 won point, 2=player 2 won point)
}

// read only properties of puck
@property (readonly) float maxSpeed;
@property (readonly) float speed;
@property (readonly) float dx;
@property (readonly) float dy;
@property (readonly) int winner;

// initialize object
-(id) initWithPuck: (UIView*) puck
          Boundary: (CGRect) boundary
             Goal1: (CGRect) goal1
             Goal2: (CGRect) goal2
          MaxSpeed: (float) max;

// reset position to middle of boundary
-(void) reset;

// returns current center position of puck
-(CGPoint) center;

// animate the puck and return true if a wall was hit
-(bool) animate;

// check for collision with paddle and alter path of puck if so
-(bool) handleCollision: (Paddle*) paddle;


@end
