//
//  TitleViewController.h
//  AirHockey
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import <UIKit/UIKit.h>

@interface TitleViewController : UIViewController

- (IBAction)onPlay:(id)sender;

@end
