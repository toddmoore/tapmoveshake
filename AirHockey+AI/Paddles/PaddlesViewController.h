//
//  PaddlesViewController.h
//  Paddles
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import <UIKit/UIKit.h>
#import "AudioToolbox/AudioToolbox.h"
#import "Paddle.h"
#import "Puck.h"

enum { AI_START, AI_WAIT, AI_BORED, AI_DEFENSE, AI_OFFENSE, AI_OFFENSE2 };

@interface PaddlesViewController : UIViewController
{
    // Paddle helpers
    Paddle *paddle1;
    Paddle *paddle2;
    Puck *puck;
     
    NSTimer *timer;
    
    UIAlertView *alert;
    
    SystemSoundID sounds[3];
    
    int state;
}

@property (retain, nonatomic) IBOutlet UIView *viewPaddle1;
@property (retain, nonatomic) IBOutlet UIView *viewPaddle2;
@property (retain, nonatomic) IBOutlet UIView *viewPuck;
@property (retain, nonatomic) IBOutlet UILabel *viewScore1;
@property (retain, nonatomic) IBOutlet UILabel *viewScore2;
@property (retain, nonatomic) IBOutlet UILabel *debug;
@property (assign) int computer;

- (void)resume;
- (void)pause;

@end
