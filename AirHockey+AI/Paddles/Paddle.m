//
//  Paddle.m
//  AirHockey
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import "Paddle.h"

@implementation Paddle

@synthesize touch;
@synthesize speed;
@synthesize maxSpeed;

-(id) initWithView: (UIView*) paddle Boundary: (CGRect) rect
          MaxSpeed: (float) max
{
    self = [super init];
    if (self)
    {
        // Custom initialization
        view = paddle;
        boundary = rect;
        maxSpeed = max;
    }
    return self;
}

//- (void)dealloc
//{
//    [super dealloc];
//}

// reset to starting position
-(void) reset
{
    pos.x = boundary.origin.x + boundary.size.width / 2;
    pos.y = boundary.origin.y + boundary.size.height / 2;
    view.center = pos;
}

// set where paddle will be moving to
-(void) move: (CGPoint) pt
{
    // adjust x position to stay within box
    if (pt.x < boundary.origin.x)
    {
        pt.x = boundary.origin.x;
    }
    else if (pt.x > boundary.origin.x + boundary.size.width)
    {
        pt.x = boundary.origin.x + boundary.size.width;
    }

    // adjust y position to stay within box
    if (pt.y < boundary.origin.y)
    {
        pt.y = boundary.origin.y;
    }
    else if (pt.y > boundary.origin.y + boundary.size.height)
    {
        pt.y = boundary.origin.y + boundary.size.height;
    }
    
    // update the position
    pos = pt;
}

// center point of paddle
-(CGPoint) center
{
    return view.center;
}

// check if the paddle intersects with the rectangle
-(bool) intersects: (CGRect) rect
{
    return CGRectIntersectsRect(view.frame, rect);
}

// get distance between current paddle position and point
-(float) distance: (CGPoint) pt
{
    float diffx = (view.center.x) - (pt.x);
    float diffy = (view.center.y) - (pt.y);
    return sqrt(diffx*diffx + diffy*diffy);
}

// animate to moveto position without exceeding max speed
-(void) animate
{
    // check if movement is needed
    if (CGPointEqualToPoint(view.center,pos) == false)
    {
        // calculate distance we need to move
        float d = [self distance: pos];
        // check the maximum distance paddle is allowed to move
        if (d > maxSpeed)
        {
            // modify the position to the max allowed
            float r = atan2(pos.y - view.center.y,
                            pos.x - view.center.x);
            float x = view.center.x + cos(r) * (maxSpeed);
            float y = view.center.y + sin(r) * (maxSpeed);
            view.center = CGPointMake(x,y);
            speed = maxSpeed;
        }
        else
        {
            // set position of paddle as it does not exceed
            // maximum speed
            view.center = pos;
            speed = d;
        }
    }
    else
    {
        // not moving
        speed = 0;
    }
}

@end
