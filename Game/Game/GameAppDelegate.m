//
//  GameAppDelegate.m
//  Game
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import "GameAppDelegate.h"

#import "GameViewController.h"

@implementation GameAppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;

// ARC Enabled
//- (void)dealloc
//{
//    [_window release];
//    [_viewController release];
//    [super dealloc];
//}

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window =
    [[UIWindow alloc]
     initWithFrame:[[UIScreen mainScreen] bounds]];
    NSLog(@"didFinishLaunchingWithOptions");
    self.viewController =
    [[GameViewController alloc]
     initWithNibName:@"GameViewController" bundle:nil];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"applicationWillResignActive");
}
- (void)applicationDidEnterBackground:
(UIApplication *)application
{
    NSLog(@"applicationDidEnterBackground");
}
- (void)applicationWillEnterForeground:
(UIApplication *)application
{
    NSLog(@"applicationWillEnterForeground");
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"applicationDidBecomeActive");
}
- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"applicationWillTerminate");
}

@end
