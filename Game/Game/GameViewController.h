//
//  GameViewController.h
//  Game
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import <UIKit/UIKit.h>

@interface GameViewController : UIViewController

@property (retain, nonatomic) IBOutlet UILabel *label;
@property (retain, nonatomic) IBOutlet UITextField *answer;

- (IBAction)submit:(id)sender;

@end
