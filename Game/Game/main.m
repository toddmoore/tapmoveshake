//
//  main.m
//  Game
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import <UIKit/UIKit.h>

#import "GameAppDelegate.h"

int main(int argc, char *argv[])
{
    NSLog(@"main");
    @autoreleasepool 
    {
        return UIApplicationMain(argc, 
                                 argv, 
                                 nil,
          NSStringFromClass([GameAppDelegate class]));
    }
}
