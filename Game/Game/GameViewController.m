//
//  GameViewController.m
//  Game
//
//  Created by Todd Moore - http://toddmoore.com/
//

#import "GameViewController.h"

@implementation GameViewController
@synthesize label;
@synthesize answer;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (void)generate
{
    // pick two numbers between 1 and 9
    int a = 1 + arc4random() % 9;
    int b = 1 + arc4random() % 9;
    // calculate the sum
    int sum = a + b;
    // create our question
    label.text = [NSString
                  stringWithFormat:
                  @"%d + %d = ", a, b];
    // save the answer in the tag property of the label
    label.tag = sum;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    
    [self generate];
}
- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear");
    [super viewWillAppear:animated];
    [self.answer becomeFirstResponder];
}
- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear");
    [super viewDidAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"viewWillDisappear");
    [super viewWillDisappear:animated];
}
- (void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"viewDidDisappear");
    [super viewDidDisappear:animated];
}
- (void)viewDidUnload
{
    NSLog(@"viewDidUnload");
    [self setLabel:nil];
    [self setAnswer:nil];
    [super viewDidUnload];
}

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}
*/

// ARC Enabled
//- (void)dealloc
//{
//    [label release];
//    [answer release];
//    [super dealloc];
//}
    
- (IBAction)submit:(id)sender
{
    // convert our answer text value into an integer
    int num = [answer.text intValue];
    // check if it is correct by comparing to the label tag
    UIAlertView *alert;
    if (num == label.tag)
    {
        // answer was correct
        alert = [[UIAlertView alloc]
                 initWithTitle:@"Correct"
                 message:@"Let’s try another one!"
                 delegate:self
                 cancelButtonTitle:@"OK"
                 otherButtonTitles: nil];
        // use the alert tag to mark that answer was correct
        alert.tag = 1;
    } else
    {
        // answer is incorrect
        alert = [[UIAlertView alloc]
                 initWithTitle:@"Wrong!"
                 message:@"That answer is incorrect."
                 delegate:self
                 cancelButtonTitle:@"Try Again"
                 otherButtonTitles: nil];
    }
    // show and release the alert
    [alert show];
    
    // ARC enabled
    //[alert release];
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
        // generate a new question
        [self generate];
        // reset our previous answer
        answer.text = @"";
    }
}

@end
